%{
#include "shop.tab.h"
void yyerror (char *s);
int yylex();
%}

alfa_id      [a-j]
numbers      [0-9]+

%%

{alfa_id}    {yylval.id = yytext[0]; return ID;}
{numbers}    {yylval.number = atoi(yytext);return NUM;}
"buy"        {return BUY;}
"discount"   {return DISC;}
"print"      {return PRINT;}
"add"        {return ADD;}
"total"      {return TOTAL;}
"pay"        {return PAY;} 
"exit"       {return EXIT;}
[ \t\n]      ;
.            {ECHO; yyerror("Unexpected Command");}

%%

int yywrap(void) {return 1;}
