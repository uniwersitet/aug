%{
    void yyerror (char *s);
    int yylex();
    #include <stdlib.h>
    #include <stdio.h>
    #include <ctype.h>
    char* products [10];
    double prices[10];
    int discounts[10];
    int magazine[10]; 
    double total;
    double buys(char id, int amount, double total, int magazine[], double prices[], int discounts[]);
    void add(char id, int amount, int magazine[]);
    void discount(char id, int amount, int discounts[]);
    void printer(char id, char* products[], double prices[], int discounts[], int magazine[]);
    int computeSymbolIndex(char token);
    double pay();
%}

%union{ 
    char id; 
    int number; 
}
%start line
%token EXIT
%token ID 
%token NUM 
%token BUY DISC PRINT TOTAL PAY ADD
%token SEMICOLON
%type  <id> ID
%type  <number> NUM

%%

line: 
    line action
    | action
    | line EXIT { total = 0.0; printf("Bye!"); exit(EXIT_SUCCESS);}
;

action:
    BUY ID NUM { total += buys($2, $3, total, magazine, prices, discounts);}
    | ADD ID NUM {add($2, $3, magazine); printf("Added to magazine!\n");}
    | DISC ID NUM {discount($2, $3, discounts);}
    | PRINT ID { printer($2, products, prices, discounts, magazine); }
    | action PAY { printf("thank you, for your purchase bye!\n"); total = 0.0;}
    | TOTAL { printf("You have to pay: %f\n", total); }
;

%%

int computeSymbolIndex(char token)
{
	int idx = -1;
    idx = token - 'a';
	return idx;
}

void yyerror (char *s) {fprintf (stderr, "%s\n", s);} 

int main()
{ 
    total = 0.00;

    products[0] = "T-shirt";
    products[1] = "Trousers";
    products[2] = "Dress";
    products[3] = "Shirt";
    products[4] = "Coat";
    products[5] = "Hoodie";
    products[6] = "Skirt";
    products[7] = "Hat";
    products[8] = "Socks";
    products[9] = "Intims";

    prices[0] = 25.99;
    prices[1] = 60.59;
    prices[2] = 200.00;
    prices[3] = 99.99;
    prices[4] = 500.10;
    prices[5] = 50.35;
    prices[6] = 75.99;
    prices[7] = 20.89;
    prices[8] = 9.99;
    prices[9] = 15.00;    

    for(int i=0; i<10; i++){
        discounts[i] = 5;
    }

    for(int i=0; i<10; i++){
        magazine[i] = 100;
    }

    yyparse();
    return 0; 
}

double buys(char id, int amount, double total, int magazine[], double prices[], int discounts[]) {
    
    int idx = computeSymbolIndex(id);
 
    if (magazine[idx] > amount){
        double disc = prices[idx] * (discounts[idx] / 100.0);
        double added = (prices[idx] - disc) * amount;
        total += added;
        magazine[idx] -= amount;
        printf("Added to the bill!\n");
    }
    else {
        printf("You cannot buy it, pieces currently in magazine: %d\n", magazine[idx]);
    }

    return total;

}

void add(char id, int amount, int magazine[]){
    int idx = computeSymbolIndex(id);
    magazine[idx] += amount;
}

void discount(char id, int amount, int discounts[]){

    if (amount < 5)
    {
        printf("You cannot discount products below 5%");
    }
    else
    {
        int idx = computeSymbolIndex(id);
        discounts[idx] = amount;
        printf("Discount changed!\n");
    }
}


void printer(char id, char* products[], double prices[], int discounts[], int magazine[])
{
    int idx = computeSymbolIndex(id);
    printf("Product: %s,\n", products[idx]); 
    printf("price zl: %f,\n", prices[idx]); 
    printf("discount: %d,\n", discounts[idx]);
    printf("in magazine: %d\n", magazine[idx]);
}